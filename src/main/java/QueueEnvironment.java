import java.util.*;

public class QueueEnvironment {
    public ArrayList<Event> events = new ArrayList<>();
    public RandomGenerator random;
    public double time = 0;
    public double lastEventTime = 0;
    public double accumulatedTime = 0;
    private final Map<String, Queue> queues = new HashMap<>();

    public void step() {
        Event event = this.events.remove(0);
        this.lastEventTime = this.time;
        this.time = event.time;
        if (event.event == Event.Type.ARRIVAL) {
            event.queue.arrival(true);
        } else if (event.event == Event.Type.DEPARTURE) {
            event.queue.departure();
            Queue destination = event.destination;
            if (destination != null) {
                destination.arrival(false);
            }
        }
    }

    public void scheduleArrival(Queue queue) {
        this.scheduleArrival(queue, this.random.nextInRange(queue.minArrival, queue.maxArrival));
    }

    public void scheduleDeparture(Queue queue, Queue destination) {
        this.scheduleDeparture(queue, this.random.nextInRange(queue.minDeparture, queue.maxDeparture), destination);
    }

    public void scheduleArrival(Queue queue, double delay) {
        Event event = new Event();
        event.time = this.time + delay;
        event.event = Event.Type.ARRIVAL;
        event.queue = queue;
        this.events.add(event);
        Collections.sort(this.events);
    }

    public void scheduleDeparture(Queue queue, double delay, Queue destination) {
        Event event = new Event();
        event.time = this.time + delay;
        event.event = Event.Type.DEPARTURE;
        event.queue = queue;
        event.destination = destination;
        this.events.add(event);
        Collections.sort(this.events);
    }

    public void recordTime() {
        for (String id : this.queues.keySet()) {
            Queue queue = this.queues.get(id);
            int population = queue.getPopulation();
            double subtotal = queue.statistics.getOrDefault(population, 0.0D);
            subtotal += this.time - this.lastEventTime;
            queue.statistics.put(population, subtotal);
        }
        this.lastEventTime = this.time;
    }

    public Queue addQueue(String id) {
        Queue queue = new Queue(id, this);
        this.queues.put(queue.getId(), queue);
        return queue;
    }

    public Queue getQueue(String id) {
        return this.queues.get(id);
    }

    public Set<String> listQueues() {
        return this.queues.keySet();
    }

}
