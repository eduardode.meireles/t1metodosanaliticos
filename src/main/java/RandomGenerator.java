import java.math.BigDecimal;
import java.math.BigInteger;

public class RandomGenerator {
    protected Long limit;
    private final long m = (long) Math.pow(2, 32);
    private BigInteger current;
    private long count;

    public RandomGenerator(Long limit, Long seed) {
        this.limit = limit;
        this.current = BigInteger.valueOf(seed);
        this.count = 0;
    }

    public boolean hasNext() {
        return this.count < this.limit;
    }

    public Double next() {
        ++this.count;
        long a = 1664525;
        long c = 1013904223;
        this.current = BigInteger.valueOf((this.current.longValue() * a + c) % this.m);
        return BigDecimal.valueOf(this.current.longValue()).divide(BigDecimal.valueOf(this.m)).doubleValue();
    }

    public double nextInRange(double min, double max) {
        double next = this.next();
        next = (max - min) * next + min;
        return next;
    }
}