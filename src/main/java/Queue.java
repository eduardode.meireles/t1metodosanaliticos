import java.util.*;

public class Queue {
    private final String id;
    public QueueEnvironment environment;
    public int population = 0;
    public int maxPopulation = -1;
    public int servers = -1;
    public double minArrival = -1;
    public double maxArrival = -1;
    public double minDeparture = -1;
    public double maxDeparture = -1;
    private final ArrayList<QueueDestination> destinations = new ArrayList<>();
    public Map<Integer, Double> statistics = new HashMap<>();
    public int lost = 0;

    public Queue(String id, QueueEnvironment env) {
        this.id = id;
        this.environment = env;
    }

    public String getId() {
        return this.id;
    }

    public void arrival(boolean cameFromStreet) {
        this.environment.recordTime();
        if (this.maxPopulation >= 0 && this.population >= this.maxPopulation) {
            ++this.lost;
        } else {
            ++this.population;
            if (this.population <= this.servers) {
                this.environment.scheduleDeparture(this, this.getDestination());
            }
        }

        if (cameFromStreet) {
            this.environment.scheduleArrival(this);
        }
    }

    public void departure() {
        this.environment.recordTime();
        --this.population;
        if (this.population >= this.servers) {
            this.environment.scheduleDeparture(this, this.getDestination());
        }
    }

    public void addDestination(Queue queue, double probability) {
        QueueDestination destination = new QueueDestination();
        destination.destination = queue;
        destination.probability = probability;
        this.destinations.add(destination);
        Collections.sort(this.destinations);
    }

    public Queue getDestination() {
        if (this.destinations.isEmpty()) {
            return null;
        } else {
            return this.destinations.size() == 1 && this.destinations.get(0).probability >= 1.0D ? this.destinations.get(0).destination : this.getDestination(this.environment.random.next());
        }
    }

    private Queue getDestination(double probabilisticIndex) {
        double tempIndex = probabilisticIndex;

        for (QueueDestination destination : this.destinations) {
            tempIndex -= destination.probability;
            if (tempIndex <= 0) {
                return destination.destination;
            }
        }

        return null;
    }

    public int getPopulation() {
        return this.population;
    }

    public String toString() {
        return this.id;
    }

    public boolean equals(Object o) {
        return o instanceof Queue && ((Queue) o).getId().equals(this.getId());
    }

    public static class QueueDestination implements Comparable<QueueDestination> {
        public double probability;
        public Queue destination;

        public int compareTo(QueueDestination other) {
            return Double.compare(this.probability, other.probability);
        }
    }
}
