import java.io.IOException;

public class Simulator {
    public static void main(String[] args) throws IOException
    {
        SimulatorParameters parameters = SimulatorParameters.read(args[0]);
        QueueEnvironment environment = parameters.initEnvironment();

        environment.random = new RandomGenerator(parameters.qtyOfRandomNumbers, parameters.seed);

        while (environment.random.hasNext()) {
            environment.step();
        }

        for (String queueId : environment.listQueues()) {
            Queue queue = environment.getQueue(queueId);
            System.out.println("Queue:   " + queue + " (Servers: " + queue.servers + ")");
            System.out.println("Service Time: " + queue.minDeparture + " - " + queue.maxDeparture);
            System.out.println("------------------------------------------------------");
            System.out.println("     State   |        Time        |    Probability    ");
            System.out.println("------------------------------------------------------");

            for (Integer state : queue.statistics.keySet()) {
                System.out.printf("%7d%21.4f%21.2f%%\n", state, queue.statistics.get(state), (queue.statistics.get(state) / (environment.time + environment.accumulatedTime)) * 100.0D);
            }

            System.out.println("\nNumber of losses: " + queue.lost + "\n");
        }

        System.out.printf("Simulation time: %f\n", (environment.time + environment.accumulatedTime));
    }
}
