import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimulatorParameters {
    public Map<String, QueueDefinition> queues = new HashMap<>();
    public List<DestinationDefinition> passages = new ArrayList<>();
    public Map<String, Double> initialArrivals = new HashMap<>();
    public Long seed = 5L;
    public Long qtyOfRandomNumbers = 100000L;

    public QueueEnvironment initEnvironment() {
        QueueEnvironment env = new QueueEnvironment();

        for (String name : this.queues.keySet()) {
            QueueDefinition queueDef = this.queues.get(name);
            Queue queue = env.addQueue(name);
            queue.maxPopulation = queueDef.capacity;
            queue.servers = queueDef.servers;
            queue.minArrival = queueDef.minArrival;
            queue.maxArrival = queueDef.maxArrival;
            queue.minDeparture = queueDef.minService;
            queue.maxDeparture = queueDef.maxService;
        }

        for (DestinationDefinition dest : this.passages) {
            env.getQueue(dest.origin).addDestination(env.getQueue(dest.destination), dest.probability);
        }

        for (String name : this.initialArrivals.keySet()) {
            env.scheduleArrival(env.getQueue(name), this.initialArrivals.get(name));
        }

        return env;
    }

    public static SimulatorParameters read(String fileName) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(fileName), SimulatorParameters.class);
    }

    public static class DestinationDefinition {
        public String origin;
        public String destination;
        public double probability;
    }

    public static class QueueDefinition {
        public int capacity = -1;
        public int servers = -1;
        public double minArrival = -1.0D;
        public double maxArrival = -1.0D;
        public double minService = -1.0D;
        public double maxService = -1.0D;
    }
}
