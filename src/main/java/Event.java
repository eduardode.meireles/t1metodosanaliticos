public class Event implements Comparable<Event>, Cloneable {
    public double time;
    public Type event;
    public Queue queue;
    public Queue destination;

    public int compareTo(Event other) {
        return Double.compare(this.time, other.time);
    }

    public enum Type {
        ARRIVAL,
        DEPARTURE;
    }
}
